import numpy as np
import tensorflow as tf
import keras
from keras import layers
import os
os.environ["CUDA_VISIBLE_DEVICES"]="1"
os.environ["TF_FORCE_GPU_ALLOW_GROWTH"]="true"

tf.config.list_physical_devices('GPU')

inputs = layers.Input(shape=(280, 1))
kernel_size = 9
conv_channels = 32
nClasses = 3
seed = 1
conv1 = layers.Conv1D(conv_channels, kernel_size, activation='relu', padding='same')(inputs)
conv1 = layers.Conv1D(conv_channels, kernel_size, activation='relu', padding='same')(conv1)
pool1 = layers.MaxPooling1D(pool_size=2)(conv1)

conv2 = layers.Conv1D(conv_channels*2, kernel_size, activation='relu', padding='same')(pool1)
conv2 = layers.Conv1D(conv_channels*2, kernel_size, activation='relu', padding='same')(conv2)
pool2 = layers.MaxPooling1D(pool_size=2)(conv2)

conv3 = layers.Conv1D(conv_channels*4, kernel_size, activation='relu', padding='same')(pool2)
conv3 = layers.Conv1D(conv_channels*4, kernel_size, activation='relu', padding='same')(conv3)
pool3 = layers.MaxPooling1D(pool_size=2)(conv3)

conv4 = layers.Conv1D(conv_channels*8, kernel_size, activation='relu', padding='same')(pool3)
conv4 = layers.Conv1D(conv_channels*8, kernel_size, activation='relu', padding='same')(conv4)

up1 = layers.Conv1D(conv_channels*4, 2, activation='relu', padding='same')(layers.UpSampling1D(size=2)(conv4))
merge1 = layers.concatenate([up1, conv3], axis=-1)
conv5 = layers.Conv1D(conv_channels*4, kernel_size, activation='relu', padding='same')(merge1)
conv5 = layers.Conv1D(conv_channels*4, kernel_size, activation='relu', padding='same')(conv5)

up2 = layers.Conv1D(conv_channels*2, 2, activation='relu', padding='same')(layers.UpSampling1D(size=2)(conv5))
merge2 = layers.concatenate([up2, conv2], axis=-1)
conv6 = layers.Conv1D(conv_channels*2, kernel_size, activation='relu', padding='same')(merge2)
conv6 = layers.Conv1D(conv_channels*2, kernel_size, activation='relu', padding='same')(conv6)

up3 = layers.Conv1D(conv_channels, 2, activation='relu', padding='same')(layers.UpSampling1D(size=2)(conv6))
merge3 = layers.concatenate([up3, conv1], axis=-1)
conv7 = layers.Conv1D(conv_channels, kernel_size, activation='relu', padding='same')(merge3)
conv7 = layers.Conv1D(conv_channels, kernel_size, activation='relu', padding='same')(conv7)

conv8 = layers.Conv1D(nClasses, 1)(conv7)
conv9 = layers.Activation('softmax')(conv8)
model = tf.keras.Model(inputs=inputs, outputs=conv9)

opt = keras.optimizers.Adam(learning_rate=1e-3)    
model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])

import tensorflow as tf
import numpy as np


def _float_feature(value):
    if isinstance(value, list):
        return tf.train.Feature(float_list=tf.train.FloatList(value=value))
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


AUTOTUNE = tf.data.experimental.AUTOTUNE

feature_description = {}

for i in range(0, 280):
    feature_description.update({'input_'+str(i) : tf.io.FixedLenFeature([], tf.float32)})
for i in range(0, 280):
    feature_description.update({'label_'+str(i)+'_0' : tf.io.FixedLenFeature([], tf.float32)})
    feature_description.update({'label_'+str(i)+'_1' : tf.io.FixedLenFeature([], tf.float32)})
    feature_description.update({'label_'+str(i)+'_2' : tf.io.FixedLenFeature([], tf.float32)})


def _parse_function(example_proto):
    dic = tf.io.parse_single_example(example_proto, feature_description)
    y = tf.stack([[dic['label_' + str(x) + '_0'], dic['label_' + str(x) + '_1'], dic['label_' + str(x) + '_2']] for x in range(0, 280)], axis=0)
    # potrebujem naparsovat vsetky tie moje blbosti, takze lets goo
    x = tf.stack([dic['input_' + str(x)] for x in range(0, 280)], axis=0)
    return x, y


train = tf.data.TFRecordDataset('./data/train_csv.ftrecords', num_parallel_reads=2)
train = train.map(_parse_function, num_parallel_calls=AUTOTUNE)
train = train.batch(batch_size=128).prefetch(buffer_size=AUTOTUNE).shuffle(buffer_size=1000000, reshuffle_each_iteration=True)

val = tf.data.TFRecordDataset('./data/valid_csv.ftrecords', num_parallel_reads=2)
val = val.map(_parse_function, num_parallel_calls=AUTOTUNE)
val = val.batch(batch_size=128).prefetch(buffer_size=AUTOTUNE)

import datetime

log_dir = "./logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath='./checkpoint_model2',
    monitor='val_loss',
    save_best_only=True)


model.fit(train, epochs=200, validation_data=val, callbacks=[tensorboard_callback, model_checkpoint_callback])
model.save('1d-unet') # tento model nebude nutne pouzity, skor sa pouzije kod pouzity pri chechpointe