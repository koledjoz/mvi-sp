## Zadanie

Cieľom práce bolo implementovať dve metódy na označenie kvality EKG. Jedna na princípe tradičnejších metód spracovania časových rád s využitím indexov kvality a druhá na princípe upravenej architektúry UNet. Tieto modely boli následne porovnané navzájom a tiež s už voľne dostupnom implementáciou v rámci knižnice [ecg_quality](https://github.com/koledjoz/ecg_quality).

## Spustenie modelov

Ukážka spustenia modelov sa nachádza v jupyter notebooku  [calculate_predictions.ipynb](https://gitlab.fit.cvut.cz/koledjoz/mvi-sp/-/blob/master/code/calculate_predictions.ipynb?ref_type=heads). Tento notebook nebude fungovať, keďže v repozitári sa nenachádzajú potrebné dáta. Nachádzajú sa v ňom však uložené modely. Okrem toho sa v repozitári nachádzajú notebooky na vyhodnotenie modelov a vizualizácie výsldkov. Taktiež je možné nájsť notebooky použité počas tvorby modelov. Tieto notebooky boli spsustené, ale niektoré nemajú zachované výstupy. Plnia skôr ukážkovú úlohu a nie je úplne mienené aby boli spúšťané.

## Použité dáta

Dáta, ktoré boli použité sa nespracované nachádzajú na [kaggle](https://www.kaggle.com/datasets/koledjoz/mvi-sp). Spôsob ako načítať jednotlivé zánamy sa nachádza v notebookoch. Nenachádza sa nikde tfrecords dataset. Tento bol síce využívaný pri tréningu ale jedná sa skôr o zápis dát v inom formáte a z neupravených dát je možné takýto dataset jednoducho vytvoriť.
